package com.example.homeworktrackingsystem.service.impl;

import com.example.homeworktrackingsystem.domain.dto.TeacherDto;
import com.example.homeworktrackingsystem.domain.exception.TeacherNotFoundException;
import com.example.homeworktrackingsystem.domain.mapper.TeacherMapper;
import com.example.homeworktrackingsystem.domain.model.Teacher;
import com.example.homeworktrackingsystem.repository.TeacherRepository;
import com.example.homeworktrackingsystem.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {
    private final TeacherRepository teacherRepository;
    private final TeacherMapper teacherMapper;

    @Override
    public Teacher createTeacher(TeacherDto teacherDto) {
        Teacher teacher = teacherMapper.toTeacher(teacherDto);
        return teacherRepository.save(teacher);
    }

    @Override
    public Teacher getTeacherById(Long id) {
        return teacherRepository.findById(id).orElseThrow(() -> new TeacherNotFoundException(id));
    }

    @Override
    public Teacher updateTeacherById(Long id, TeacherDto teacherDto) {
        Teacher teacher = teacherRepository.findById(id).orElseThrow(() -> new TeacherNotFoundException(id));
        teacher.setName(teacherDto.getName());
        teacher.setSurname(teacherDto.getSurname());
        teacher.setPatronymic(teacherDto.getPatronymic());
        return teacherRepository.save(teacher);
    }

    @Override
    public String deleteTeacherById(Long id) {
        teacherRepository.deleteById(id);
        return String.format("teacher with id: %d successfully deleted", id);
    }
}

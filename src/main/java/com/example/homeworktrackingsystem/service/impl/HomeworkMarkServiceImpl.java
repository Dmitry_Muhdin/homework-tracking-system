package com.example.homeworktrackingsystem.service.impl;

import com.example.homeworktrackingsystem.domain.dto.HomeworkMarkDto;
import com.example.homeworktrackingsystem.domain.exception.HomeworkMarkNotFoundException;
import com.example.homeworktrackingsystem.domain.exception.MarkForHomeworkAlreadyExistException;
import com.example.homeworktrackingsystem.domain.mapper.HomeworkMarkMapper;
import com.example.homeworktrackingsystem.domain.model.HomeworkMark;
import com.example.homeworktrackingsystem.repository.HomeworkMarkRepository;
import com.example.homeworktrackingsystem.service.HomeworkMarkService;
import com.example.homeworktrackingsystem.service.HomeworkService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HomeworkMarkServiceImpl implements HomeworkMarkService {
    private final HomeworkMarkMapper homeworkMarkMapper;
    private final HomeworkMarkRepository homeworkMarkRepository;
    private final HomeworkService homeworkService;

    @Override
    public HomeworkMark createHomeworkMark(HomeworkMarkDto homeworkMarkDto) {
        homeworkMarkRepository
                .findHomeworkMarkByHomeworkId(homeworkMarkDto.getHomeworkId())
                .ifPresent((v) -> {
                    throw new MarkForHomeworkAlreadyExistException(homeworkMarkDto.getHomeworkId());
                });
        HomeworkMark homeworkMark = homeworkMarkMapper.toHomeworkMark(homeworkMarkDto);
        return homeworkMarkRepository.save(homeworkMark);
    }

    @Override
    public HomeworkMark getHomeworkMarkById(Long id) {
        return homeworkMarkRepository.findById(id).orElseThrow(() -> new HomeworkMarkNotFoundException(id));
    }

    @Override
    public HomeworkMark updateHomeworkMarkById(Long id, HomeworkMarkDto homeworkMarkDto) {
        HomeworkMark homeworkMark = getHomeworkMarkById(id);
        homeworkMark.setHomework(homeworkService.getHomeWorkById(homeworkMarkDto.getHomeworkId()));
        homeworkMark.setMark(homeworkMarkDto.getMark());
        homeworkMark.setComment(homeworkMarkDto.getComment());
        return homeworkMarkRepository.save(homeworkMark);
    }

    @Override
    public String deleteHomeworkMarkById(Long id) {
        homeworkMarkRepository.deleteById(id);
        return String.format("homework mark with id: %d not found exception", id);
    }

    @Override
    public List<HomeworkMark> getHomeworkMarksByStudentId(Long studentId) {
        return homeworkMarkRepository.findHomeworkMarksByStudentId(studentId);
    }
}

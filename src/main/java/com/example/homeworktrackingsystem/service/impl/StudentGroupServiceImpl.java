package com.example.homeworktrackingsystem.service.impl;

import com.example.homeworktrackingsystem.domain.dto.StudentGroupDto;
import com.example.homeworktrackingsystem.domain.exception.GroupNotFoundException;
import com.example.homeworktrackingsystem.domain.mapper.StudentGroupMapper;
import com.example.homeworktrackingsystem.domain.model.StudentGroup;
import com.example.homeworktrackingsystem.repository.StudentGroupRepository;
import com.example.homeworktrackingsystem.service.StudentGroupService;
import com.example.homeworktrackingsystem.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentGroupServiceImpl implements StudentGroupService {
    private final StudentGroupRepository studentGroupRepository;
    private final StudentGroupMapper studentGroupMapper;
    private final TeacherService teacherService;

    @Override
    public StudentGroup getGroupById(Long id) {
        return studentGroupRepository.findById(id).orElseThrow(() -> new GroupNotFoundException(id));
    }

    @Override
    public StudentGroup createGroup(StudentGroupDto studentGroupDto) {
        StudentGroup studentGroup = studentGroupMapper.toGroup(studentGroupDto);
        return studentGroupRepository.save(studentGroup);
    }

    @Override
    public StudentGroup updateGroupById(Long id, StudentGroupDto studentGroupDto) {
        StudentGroup studentGroup = studentGroupRepository.findById(id).orElseThrow(() -> new GroupNotFoundException(id));
        studentGroup.setGroupName(studentGroupDto.getGroupName());
        studentGroup.setCurator(teacherService.getTeacherById(id));
        return studentGroupRepository.save(studentGroup);
    }

    @Override
    public String deleteGroupById(Long id) {
        studentGroupRepository.deleteById(id);
        return String.format("group with id: %d successfully deleted", id);
    }
}

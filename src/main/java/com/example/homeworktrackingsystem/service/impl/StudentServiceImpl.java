package com.example.homeworktrackingsystem.service.impl;

import com.example.homeworktrackingsystem.domain.dto.StudentDto;
import com.example.homeworktrackingsystem.domain.exception.StudentNotFoundException;
import com.example.homeworktrackingsystem.domain.mapper.StudentMapper;
import com.example.homeworktrackingsystem.domain.model.Student;
import com.example.homeworktrackingsystem.repository.StudentRepository;
import com.example.homeworktrackingsystem.service.StudentGroupService;
import com.example.homeworktrackingsystem.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;
    private final StudentGroupService studentGroupService;

    @Override
    public Student createStudent(StudentDto studentDto) {
        Student student = studentMapper.toStudent(studentDto);
        return studentRepository.save(student);
    }

    @Override
    public Student getStudentById(Long id) {
        return studentRepository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));
    }

    @Override
    public List<Student> getStudentsByGroupId(Long groupId) {
        return studentRepository.findStudentsByGroupId(groupId);
    }

    @Override
    public Student updateStudentById(Long id, StudentDto studentDto) {
        Student studentById = getStudentById(id);
        studentById.setName(studentDto.getName());
        studentById.setStudentGroup(studentGroupService.getGroupById(studentDto.getGroupId()));
        studentById.setSurname(studentDto.getSurname());
        studentById.setCreditBookNumber(studentDto.getCreditBookNumber());
        return studentRepository.save(studentById);
    }

    @Override
    public String deleteStudentById(Long id) {
        studentRepository.deleteById(id);
        return String.format("student with id: %d successfully deleted", id);
    }
}

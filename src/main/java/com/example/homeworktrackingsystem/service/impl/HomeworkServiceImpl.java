package com.example.homeworktrackingsystem.service.impl;

import com.example.homeworktrackingsystem.domain.dto.HomeworkDto;
import com.example.homeworktrackingsystem.domain.exception.HomeworkNotFoundException;
import com.example.homeworktrackingsystem.domain.mapper.HomeworkMapper;
import com.example.homeworktrackingsystem.domain.model.Homework;
import com.example.homeworktrackingsystem.repository.HomeworkRepository;
import com.example.homeworktrackingsystem.service.HomeworkService;
import com.example.homeworktrackingsystem.service.StudentService;
import com.example.homeworktrackingsystem.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class HomeworkServiceImpl implements HomeworkService {
    private final HomeworkRepository homeworkRepository;
    private final HomeworkMapper homeworkMapper;
    private final StudentService studentService;
    private final TeacherService teacherService;

    @Override
    public Homework createHomework(HomeworkDto homeworkDto) {
        Homework homework = homeworkMapper.toHomework(homeworkDto);
        return homeworkRepository.save(homework);
    }

    @Override
    public Homework getHomeWorkById(Long id) {
        return homeworkRepository.findById(id).orElseThrow(() -> new HomeworkNotFoundException(id));
    }

    @Override
    public List<Homework> getHomeworksByStudentId(Long studentId) {
        return homeworkRepository.findHomeworksByStudentId(studentId);
    }

    @Override
    public List<Homework> getHomeworksByTeacherId(Long teacherId) {
        return homeworkRepository.findHomeworksByTeacherId(teacherId);
    }

    @Override
    public Homework updateHomeworkById(Long id, HomeworkDto homeworkDto) {
        Homework homeWorkById = getHomeWorkById(id);
        homeWorkById.setTask(homeworkDto.getTask());
        homeWorkById.setDeadline(homeworkDto.getDeadline());
        homeWorkById.setTeacher(teacherService.getTeacherById(homeworkDto.getTeacherId()));
        homeWorkById.setStudent(studentService.getStudentById(homeworkDto.getStudentId()));
        return homeworkRepository.save(homeWorkById);
    }

    @Override
    public String deleteHomeworkById(Long id) {
        homeworkRepository.deleteById(id);
        return String.format("homework with id: %d successfully deleted", id);
    }
}

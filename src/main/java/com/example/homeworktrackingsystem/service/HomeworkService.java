package com.example.homeworktrackingsystem.service;

import com.example.homeworktrackingsystem.domain.dto.HomeworkDto;
import com.example.homeworktrackingsystem.domain.model.Homework;

import java.util.List;

public interface HomeworkService {
    Homework createHomework(HomeworkDto homeworkDto);

    Homework getHomeWorkById(Long id);

    List<Homework> getHomeworksByStudentId(Long studentId);

    List<Homework> getHomeworksByTeacherId(Long teacherId);

    Homework updateHomeworkById(Long id, HomeworkDto homeworkDto);

    String deleteHomeworkById(Long id);
}

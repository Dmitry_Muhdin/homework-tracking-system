package com.example.homeworktrackingsystem.service;

import com.example.homeworktrackingsystem.domain.dto.TeacherDto;
import com.example.homeworktrackingsystem.domain.model.Teacher;

public interface TeacherService {
    Teacher createTeacher(TeacherDto teacherDto);

    Teacher getTeacherById(Long id);

    Teacher updateTeacherById(Long id, TeacherDto teacherDto);

    String deleteTeacherById(Long id);
}

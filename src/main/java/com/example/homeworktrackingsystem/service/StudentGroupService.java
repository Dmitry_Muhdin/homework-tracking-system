package com.example.homeworktrackingsystem.service;

import com.example.homeworktrackingsystem.domain.dto.StudentGroupDto;
import com.example.homeworktrackingsystem.domain.model.StudentGroup;

public interface StudentGroupService {
    StudentGroup getGroupById(Long id);

    StudentGroup createGroup(StudentGroupDto studentGroupDto);

    StudentGroup updateGroupById(Long id, StudentGroupDto studentGroupDto);

    String deleteGroupById(Long id);
}

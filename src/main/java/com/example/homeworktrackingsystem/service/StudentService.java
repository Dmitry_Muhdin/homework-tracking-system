package com.example.homeworktrackingsystem.service;

import com.example.homeworktrackingsystem.domain.dto.StudentDto;
import com.example.homeworktrackingsystem.domain.model.Student;

import java.util.List;

public interface StudentService {
    Student createStudent(StudentDto studentDto);

    Student getStudentById(Long id);

    List<Student> getStudentsByGroupId(Long groupId);

    Student updateStudentById(Long id, StudentDto studentDto);

    String deleteStudentById(Long id);
}

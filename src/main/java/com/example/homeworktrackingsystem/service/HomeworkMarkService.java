package com.example.homeworktrackingsystem.service;

import com.example.homeworktrackingsystem.domain.dto.HomeworkMarkDto;
import com.example.homeworktrackingsystem.domain.model.HomeworkMark;

import java.util.List;

public interface HomeworkMarkService {
    HomeworkMark createHomeworkMark(HomeworkMarkDto homeworkMarkDto);

    HomeworkMark getHomeworkMarkById(Long id);

    HomeworkMark updateHomeworkMarkById(Long id, HomeworkMarkDto homeworkMarkDto);

    String deleteHomeworkMarkById(Long id);

    List<HomeworkMark> getHomeworkMarksByStudentId(Long studentId);
}

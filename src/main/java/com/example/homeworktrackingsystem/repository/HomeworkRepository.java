package com.example.homeworktrackingsystem.repository;

import com.example.homeworktrackingsystem.domain.model.Homework;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HomeworkRepository extends JpaRepository<Homework, Long> {
    @Query("select h from Homework h where h.student.id = :student_id")
    List<Homework> findHomeworksByStudentId(@Param("student_id") Long studentId);

    @Query("select h from Homework h where h.teacher.id = :teacher_id")
    List<Homework> findHomeworksByTeacherId(@Param("teacher_id") Long teacherId);
}

package com.example.homeworktrackingsystem.repository;

import com.example.homeworktrackingsystem.domain.model.HomeworkMark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface HomeworkMarkRepository extends JpaRepository<HomeworkMark, Long> {
    Optional<HomeworkMark> findHomeworkMarkByHomeworkId(Long id);

    @Query("select hm from HomeworkMark hm where hm.homework.student.id = :student_id")
    List<HomeworkMark> findHomeworkMarksByStudentId(@Param("student_id") Long studentId);
}

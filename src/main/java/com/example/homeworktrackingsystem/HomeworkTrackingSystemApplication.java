package com.example.homeworktrackingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkTrackingSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomeworkTrackingSystemApplication.class, args);
    }

}

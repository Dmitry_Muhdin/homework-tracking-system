package com.example.homeworktrackingsystem.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeacherDto {
    String name;
    String surname;
    String patronymic;
}

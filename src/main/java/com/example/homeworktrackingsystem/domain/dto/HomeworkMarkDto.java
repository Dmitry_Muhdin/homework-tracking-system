package com.example.homeworktrackingsystem.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HomeworkMarkDto {
    int mark;
    String comment;
    Long homeworkId;
}

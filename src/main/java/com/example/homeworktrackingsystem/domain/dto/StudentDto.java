package com.example.homeworktrackingsystem.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentDto {
    String name;
    String surname;
    String creditBookNumber;
    Long groupId;
}

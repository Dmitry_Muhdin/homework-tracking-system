package com.example.homeworktrackingsystem.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class HomeworkDto {
    String task;
    LocalDateTime deadline;
    Long teacherId;
    Long studentId;
}

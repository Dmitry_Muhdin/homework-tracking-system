package com.example.homeworktrackingsystem.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentGroupDto {
    String groupName;
    Long curatorId;
}

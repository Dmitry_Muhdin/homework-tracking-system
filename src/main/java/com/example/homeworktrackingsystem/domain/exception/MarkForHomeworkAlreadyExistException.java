package com.example.homeworktrackingsystem.domain.exception;

public class MarkForHomeworkAlreadyExistException extends RuntimeException {
    public MarkForHomeworkAlreadyExistException(Long homeworkId) {
        super(String.format("mark for homework with id: %d already exist", homeworkId));
    }
}

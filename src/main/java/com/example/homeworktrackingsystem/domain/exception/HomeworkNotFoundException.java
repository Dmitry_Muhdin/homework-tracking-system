package com.example.homeworktrackingsystem.domain.exception;

public class HomeworkNotFoundException extends RuntimeException {
    public HomeworkNotFoundException(Long id) {
        super(String.format("Homework with id: %d not found exception", id));
    }
}

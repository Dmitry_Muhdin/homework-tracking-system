package com.example.homeworktrackingsystem.domain.exception;

public class HomeworkMarkNotFoundException extends RuntimeException {
    public HomeworkMarkNotFoundException(Long id) {
        super(String.format("homework mark with id: %d not found", id));
    }
}

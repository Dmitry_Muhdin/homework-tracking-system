package com.example.homeworktrackingsystem.domain.exception;

public class GroupNotFoundException extends RuntimeException {
    public GroupNotFoundException(Long id) {
        super(String.format("Group with id: %d not found", id));
    }
}

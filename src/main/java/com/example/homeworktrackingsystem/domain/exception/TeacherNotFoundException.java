package com.example.homeworktrackingsystem.domain.exception;

public class TeacherNotFoundException extends RuntimeException {
    public TeacherNotFoundException(Long id) {
        super(String.format("teacher with id: %d not found", id));
    }
}

package com.example.homeworktrackingsystem.domain.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "group_generator", sequenceName = "student_group_sequence")
@Table(name = "STUDENT_GROUP")
public class StudentGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "group_generator")
    Long id;
    @Column(name = "group_name")
    String groupName;
    @ManyToOne
    @JoinColumn(name = "curator_teacher_id")
    Teacher curator;
}

package com.example.homeworktrackingsystem.domain.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "homework_generator", sequenceName = "homework_sequence")
@Table(name = "HOMEWORK")
public class Homework {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "homework_generator")
    Long id;
    @Column(name = "task")
    String task;
    @Column(name = "deadline")
    LocalDateTime deadline;
    @ManyToOne
    @JoinColumn(name = "teacher_id")
    Teacher teacher;
    @ManyToOne
    @JoinColumn(name = "student_id")
    Student student;
    @OneToOne(mappedBy = "homework")
    @JoinColumn(name = "homework_mark")
    HomeworkMark homeworkMark;
}

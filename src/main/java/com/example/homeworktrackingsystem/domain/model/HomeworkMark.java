package com.example.homeworktrackingsystem.domain.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "homework_mark_generator", sequenceName = "homework_mark_sequence")
@Table(name = "HOME_WORK_MARK")
public class HomeworkMark {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "homework_mark_generator")
    Long id;
    @Column(name = "mark")
    int mark;
    @Column(name = "comment")
    String comment;
    @OneToOne
    Homework homework;
}

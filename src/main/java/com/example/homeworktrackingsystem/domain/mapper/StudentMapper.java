package com.example.homeworktrackingsystem.domain.mapper;

import com.example.homeworktrackingsystem.domain.dto.StudentDto;
import com.example.homeworktrackingsystem.domain.exception.GroupNotFoundException;
import com.example.homeworktrackingsystem.domain.model.Student;
import com.example.homeworktrackingsystem.domain.model.StudentGroup;
import com.example.homeworktrackingsystem.repository.StudentGroupRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class StudentMapper {
    @Autowired
    private StudentGroupRepository studentGroupRepository;

    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "creditBookNumber", source = "creditBookNumber")
    @Mapping(target = "studentGroup", source = "groupId", qualifiedByName = "findGroup")
    public abstract Student toStudent(StudentDto studentDto);

    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "creditBookNumber", source = "creditBookNumber")
    @Mapping(target = "groupId", source = "studentGroup.id")
    public abstract StudentDto toStudentDto(Student student);

    @Named("findGroup")
    StudentGroup findGroup(Long id) {
        return studentGroupRepository.findById(id).orElseThrow(() -> new GroupNotFoundException(id));
    }
}

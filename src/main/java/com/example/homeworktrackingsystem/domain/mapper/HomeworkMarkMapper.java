package com.example.homeworktrackingsystem.domain.mapper;

import com.example.homeworktrackingsystem.domain.dto.HomeworkMarkDto;
import com.example.homeworktrackingsystem.domain.exception.HomeworkNotFoundException;
import com.example.homeworktrackingsystem.domain.model.Homework;
import com.example.homeworktrackingsystem.domain.model.HomeworkMark;
import com.example.homeworktrackingsystem.repository.HomeworkRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class HomeworkMarkMapper {
    @Autowired
    private HomeworkRepository homeworkRepository;

    @Mapping(target = "mark", source = "mark")
    @Mapping(target = "comment", source = "comment")
    @Mapping(target = "homework", source = "homeworkId", qualifiedByName = "findHomework")
    public abstract HomeworkMark toHomeworkMark(HomeworkMarkDto homeworkMarkDto);

    @Mapping(target = "mark", source = "mark")
    @Mapping(target = "comment", source = "comment")
    @Mapping(target = "homeworkId", source = "homework.id")
    public abstract HomeworkMarkDto toHomeworkMarkDto(HomeworkMark homeworkMark);

    @Named("findHomework")
    Homework findHomework(Long id) {
        return homeworkRepository.findById(id).orElseThrow(() -> new HomeworkNotFoundException(id));
    }
}

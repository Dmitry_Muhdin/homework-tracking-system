package com.example.homeworktrackingsystem.domain.mapper;

import com.example.homeworktrackingsystem.domain.dto.HomeworkDto;
import com.example.homeworktrackingsystem.domain.exception.StudentNotFoundException;
import com.example.homeworktrackingsystem.domain.exception.TeacherNotFoundException;
import com.example.homeworktrackingsystem.domain.model.Homework;
import com.example.homeworktrackingsystem.domain.model.Student;
import com.example.homeworktrackingsystem.domain.model.Teacher;
import com.example.homeworktrackingsystem.repository.HomeworkMarkRepository;
import com.example.homeworktrackingsystem.repository.StudentRepository;
import com.example.homeworktrackingsystem.repository.TeacherRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class HomeworkMapper {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private TeacherRepository teacherRepository;

    @Mapping(target = "task", source = "task")
    @Mapping(target = "deadline", source = "deadline")
    @Mapping(target = "teacherId", source = "teacher.id")
    @Mapping(target = "studentId", source = "student.id")
    public abstract HomeworkDto toHomeworkDto(Homework homework);

    @Mapping(target = "task", source = "task")
    @Mapping(target = "deadline", source = "deadline")
    @Mapping(target = "teacher", source = "teacherId", qualifiedByName = "findTeacher")
    @Mapping(target = "student", source = "studentId", qualifiedByName = "findStudent")
    public abstract Homework toHomework(HomeworkDto homeworkDto);

    @Named("findTeacher")
    Teacher findTeacher(Long id) {
        return teacherRepository.findById(id).orElseThrow(() -> new TeacherNotFoundException(id));
    }

    @Named("findStudent")
    Student findStudent(Long id) {
        return studentRepository.findById(id).orElseThrow(() -> new StudentNotFoundException(id));
    }
}

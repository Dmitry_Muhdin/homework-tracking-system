package com.example.homeworktrackingsystem.domain.mapper;

import com.example.homeworktrackingsystem.domain.dto.TeacherDto;
import com.example.homeworktrackingsystem.domain.model.Teacher;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TeacherMapper {
    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "patronymic", source = "patronymic")
    Teacher toTeacher(TeacherDto teacherDto);

    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "patronymic", source = "patronymic")
    TeacherDto toTeacherDto(Teacher teacher);
}

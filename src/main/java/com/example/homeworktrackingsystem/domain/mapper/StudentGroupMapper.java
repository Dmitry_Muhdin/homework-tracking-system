package com.example.homeworktrackingsystem.domain.mapper;

import com.example.homeworktrackingsystem.domain.dto.StudentGroupDto;
import com.example.homeworktrackingsystem.domain.exception.TeacherNotFoundException;
import com.example.homeworktrackingsystem.domain.model.StudentGroup;
import com.example.homeworktrackingsystem.domain.model.Teacher;
import com.example.homeworktrackingsystem.repository.TeacherRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class StudentGroupMapper {
    @Autowired
    private TeacherRepository teacherRepository;

    @Mapping(source = "groupName", target = "groupName")
    @Mapping(source = "curator.id", target = "curatorId")
    public abstract StudentGroupDto toGroupDto(StudentGroup studentGroup);

    @Mapping(source = "groupName", target = "groupName")
    @Mapping(source = "curatorId", target = "curator", qualifiedByName = "findTeacher")
    public abstract StudentGroup toGroup(StudentGroupDto studentGroupDto);

    @Named("findTeacher")
    Teacher findTeacher(Long id) {
        return teacherRepository.findById(id).orElseThrow(() -> new TeacherNotFoundException(id));
    }
}

package com.example.homeworktrackingsystem.controller.impl;

import com.example.homeworktrackingsystem.controller.StudentGroupController;
import com.example.homeworktrackingsystem.domain.dto.StudentGroupDto;
import com.example.homeworktrackingsystem.domain.mapper.StudentGroupMapper;
import com.example.homeworktrackingsystem.domain.model.StudentGroup;
import com.example.homeworktrackingsystem.service.StudentGroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/groups")
@RequiredArgsConstructor
public class StudentGroupControllerImpl implements StudentGroupController {
    private final StudentGroupService studentGroupService;
    private final StudentGroupMapper studentGroupMapper;

    @Override
    @PostMapping
    public StudentGroupDto createGroup(@RequestBody StudentGroupDto studentGroupDto) {
        StudentGroup createdStudentGroup = studentGroupService.createGroup(studentGroupDto);
        return studentGroupMapper.toGroupDto(createdStudentGroup);
    }

    @Override
    @GetMapping("/{id}")
    public StudentGroupDto getGroupById(@PathVariable Long id) {
        StudentGroup studentGroupById = studentGroupService.getGroupById(id);
        return studentGroupMapper.toGroupDto(studentGroupById);
    }

    @Override
    @PutMapping("/{id}")
    public StudentGroupDto updateGroupById(@PathVariable Long id, @RequestBody StudentGroupDto studentGroupDto) {
        StudentGroup studentGroup = studentGroupService.updateGroupById(id, studentGroupDto);
        return studentGroupMapper.toGroupDto(studentGroup);
    }

    @Override
    @DeleteMapping("/{id}")
    public String deleteGroupById(@PathVariable Long id) {
        return studentGroupService.deleteGroupById(id);
    }
}

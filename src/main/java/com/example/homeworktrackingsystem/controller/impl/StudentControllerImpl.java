package com.example.homeworktrackingsystem.controller.impl;

import com.example.homeworktrackingsystem.controller.StudentController;
import com.example.homeworktrackingsystem.domain.dto.StudentDto;
import com.example.homeworktrackingsystem.domain.mapper.StudentMapper;
import com.example.homeworktrackingsystem.domain.model.Student;
import com.example.homeworktrackingsystem.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentControllerImpl implements StudentController {
    private final StudentService studentService;
    private final StudentMapper studentMapper;

    @Override
    @PostMapping
    public StudentDto createStudent(@RequestBody StudentDto studentDto) {
        Student student = studentService.createStudent(studentDto);
        return studentMapper.toStudentDto(student);
    }

    @Override
    @GetMapping("/{id}")
    public StudentDto getStudentById(@PathVariable Long id) {
        Student studentById = studentService.getStudentById(id);
        return studentMapper.toStudentDto(studentById);
    }

    @Override
    @GetMapping
    public List<StudentDto> getStudentsByGroupId(@RequestParam("group-id") Long groupId) {
        List<Student> studentsByGroupId = studentService.getStudentsByGroupId(groupId);
        return studentsByGroupId.stream().map(studentMapper::toStudentDto).collect(Collectors.toList());
    }

    @Override
    @PutMapping("/{id}")
    public StudentDto updateStudentById(@PathVariable Long id, @RequestBody StudentDto studentDto) {
        Student student = studentService.updateStudentById(id, studentDto);
        return studentMapper.toStudentDto(student);
    }

    @Override
    @DeleteMapping("/{id}")
    public String deleteStudentById(@PathVariable Long id) {
        return studentService.deleteStudentById(id);
    }
}

package com.example.homeworktrackingsystem.controller.impl;

import com.example.homeworktrackingsystem.controller.TeacherController;
import com.example.homeworktrackingsystem.domain.dto.TeacherDto;
import com.example.homeworktrackingsystem.domain.mapper.TeacherMapper;
import com.example.homeworktrackingsystem.domain.model.Teacher;
import com.example.homeworktrackingsystem.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/teachers")
@RequiredArgsConstructor
public class TeacherControllerImpl implements TeacherController {
    private final TeacherService teacherService;
    private final TeacherMapper teacherMapper;

    @Override
    @PostMapping
    public TeacherDto createTeacher(@RequestBody TeacherDto teacherDto) {
        Teacher teacher = teacherService.createTeacher(teacherDto);
        return teacherMapper.toTeacherDto(teacher);
    }

    @Override
    @GetMapping("/{id}")
    public TeacherDto getTeacherById(@PathVariable Long id) {
        Teacher teacherById = teacherService.getTeacherById(id);
        return teacherMapper.toTeacherDto(teacherById);
    }

    @Override
    @PutMapping("/{id}")
    public TeacherDto updateTeacherById(@PathVariable Long id, @RequestBody TeacherDto teacherDto) {
        Teacher teacher = teacherService.updateTeacherById(id, teacherDto);
        return teacherMapper.toTeacherDto(teacher);
    }

    @Override
    @DeleteMapping("/{id}")
    public String deleteTeacherById(@PathVariable Long id) {
        return teacherService.deleteTeacherById(id);
    }
}

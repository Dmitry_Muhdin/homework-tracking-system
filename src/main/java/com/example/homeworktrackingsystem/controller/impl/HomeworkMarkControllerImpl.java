package com.example.homeworktrackingsystem.controller.impl;

import com.example.homeworktrackingsystem.controller.HomeworkMarkController;
import com.example.homeworktrackingsystem.domain.dto.HomeworkMarkDto;
import com.example.homeworktrackingsystem.domain.mapper.HomeworkMarkMapper;
import com.example.homeworktrackingsystem.domain.model.HomeworkMark;
import com.example.homeworktrackingsystem.service.HomeworkMarkService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/homework-marks")
@RequiredArgsConstructor
public class HomeworkMarkControllerImpl implements HomeworkMarkController {
    private final HomeworkMarkService homeworkMarkService;
    private final HomeworkMarkMapper homeworkMarkMapper;

    @Override
    @PostMapping
    public HomeworkMarkDto createHomeworkMark(@RequestBody HomeworkMarkDto homeworkMarkDto) {
        HomeworkMark homeworkMark = homeworkMarkService.createHomeworkMark(homeworkMarkDto);
        return homeworkMarkMapper.toHomeworkMarkDto(homeworkMark);
    }

    @GetMapping("/{id}")
    @Override
    public HomeworkMarkDto getHomeworkMarkById(@PathVariable Long id) {
        HomeworkMark homeworkMark = homeworkMarkService.getHomeworkMarkById(id);
        return homeworkMarkMapper.toHomeworkMarkDto(homeworkMark);
    }

    @Override
    @GetMapping
    public List<HomeworkMarkDto> getMarksByStudentId(@RequestParam("student-id") Long studentId) {
        return homeworkMarkService.getHomeworkMarksByStudentId(studentId).stream()
                .map(homeworkMarkMapper::toHomeworkMarkDto)
                .collect(Collectors.toList());
    }

    @Override
    @PutMapping("/{id}")
    public HomeworkMarkDto updateHomeworkMarkById(@PathVariable Long id, @RequestBody HomeworkMarkDto homeworkMarkDto) {
        HomeworkMark homeworkMark = homeworkMarkService.updateHomeworkMarkById(id, homeworkMarkDto);
        return homeworkMarkMapper.toHomeworkMarkDto(homeworkMark);
    }

    @Override
    @DeleteMapping("/{id}")
    public String deleteHomeworkMarkById(@PathVariable Long id) {
        return homeworkMarkService.deleteHomeworkMarkById(id);
    }
}

package com.example.homeworktrackingsystem.controller.impl;

import com.example.homeworktrackingsystem.controller.HomeworkController;
import com.example.homeworktrackingsystem.domain.dto.HomeworkDto;
import com.example.homeworktrackingsystem.domain.mapper.HomeworkMapper;
import com.example.homeworktrackingsystem.domain.model.Homework;
import com.example.homeworktrackingsystem.service.HomeworkService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/homeworks")
@RequiredArgsConstructor
public class HomeworkControllerImpl implements HomeworkController {
    private final HomeworkService homeworkService;
    private final HomeworkMapper homeworkMapper;

    @Override
    @PostMapping
    public HomeworkDto createHomework(@RequestBody HomeworkDto homeworkDto) {
        Homework homework = homeworkService.createHomework(homeworkDto);
        return homeworkMapper.toHomeworkDto(homework);
    }

    @Override
    @GetMapping("/{id}")
    public HomeworkDto getHomeWorkById(@PathVariable Long id) {
        Homework homeWorkById = homeworkService.getHomeWorkById(id);
        return homeworkMapper.toHomeworkDto(homeWorkById);
    }

    @Override
    @GetMapping(params = "student-id")
    public List<HomeworkDto> getHomeworksByStudentId(@RequestParam("student-id") Long studentId) {
        List<Homework> homeworksByStudentId = homeworkService.getHomeworksByStudentId(studentId);
        return homeworksByStudentId.stream().map(homeworkMapper::toHomeworkDto).collect(Collectors.toList());
    }

    @Override
    @GetMapping(params = "teacher-id")
    public List<HomeworkDto> getHomeworksByTeacherId(@RequestParam("teacher-id") Long teacherId) {
        List<Homework> homeworksByTeacherId = homeworkService.getHomeworksByTeacherId(teacherId);
        return homeworksByTeacherId.stream().map(homeworkMapper::toHomeworkDto).collect(Collectors.toList());
    }

    @Override
    @PutMapping("/{id}")
    public HomeworkDto updateHomeworkById(@PathVariable Long id, @RequestBody HomeworkDto homeworkDto) {
        Homework homework = homeworkService.updateHomeworkById(id, homeworkDto);
        return homeworkMapper.toHomeworkDto(homework);
    }

    @Override
    @DeleteMapping("/{id}")
    public String deleteHomeworkById(@PathVariable Long id) {
        return homeworkService.deleteHomeworkById(id);
    }
}

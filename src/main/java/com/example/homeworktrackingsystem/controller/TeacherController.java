package com.example.homeworktrackingsystem.controller;

import com.example.homeworktrackingsystem.domain.dto.TeacherDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Преподаватели", description = "Контроллер для работы с преподавателями")
public interface TeacherController {
    @Operation(method = "POST", description = "создать преподавателя")
    TeacherDto createTeacher(TeacherDto teacherDto);

    @Operation(method = "GET", description = "получить преподавателя по id")
    TeacherDto getTeacherById(Long id);

    @Operation(method = "PUT", description = "обновить преподавателя по id")
    TeacherDto updateTeacherById(Long id, TeacherDto teacherDto);

    @Operation(method = "DELETE", description = "удалить преподавателя по id")
    String deleteTeacherById(Long id);
}

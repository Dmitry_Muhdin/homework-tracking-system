package com.example.homeworktrackingsystem.controller;

import com.example.homeworktrackingsystem.domain.dto.StudentDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

@Tag(name = "Студенты", description = "Контроллер для работы со студентами")
public interface StudentController {
    @Operation(method = "POST", description = "создать студента")
    StudentDto createStudent(StudentDto studentDto);

    @Operation(method = "GET", description = "получить студента по id")
    StudentDto getStudentById(Long id);

    @Operation(method = "GET", description = "получить студентов по id группы")
    List<StudentDto> getStudentsByGroupId(Long groupId);

    @Operation(method = "PUT", description = "обновить студента по id")
    StudentDto updateStudentById(Long id, StudentDto studentDto);

    @Operation(method = "DELETE", description = "удалить студента по id")
    String deleteStudentById(Long id);
}

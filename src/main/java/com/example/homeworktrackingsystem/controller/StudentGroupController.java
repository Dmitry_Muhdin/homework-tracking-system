package com.example.homeworktrackingsystem.controller;

import com.example.homeworktrackingsystem.domain.dto.StudentGroupDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Учебные группы", description = "Контроллер для работы с учебными группами")
public interface StudentGroupController {
    @Operation(method = "POST", description = "создать группу")
    StudentGroupDto createGroup(StudentGroupDto studentGroupDto);

    @Operation(method = "GET", description = "получить группу по id")
    StudentGroupDto getGroupById(Long id);

    @Operation(method = "PUT", description = "обновить группу по id")
    StudentGroupDto updateGroupById(Long id, StudentGroupDto studentGroupDto);

    @Operation(method = "DELETE", description = "удалить группу по id")
    String deleteGroupById(Long id);
}

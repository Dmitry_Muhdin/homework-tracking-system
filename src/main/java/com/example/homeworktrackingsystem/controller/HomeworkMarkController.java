package com.example.homeworktrackingsystem.controller;

import com.example.homeworktrackingsystem.domain.dto.HomeworkMarkDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

@Tag(name = "Оценки домашних заданий", description = "Контроллер для работы с домашними заданиями")
public interface HomeworkMarkController {
    @Operation(method = "POST", description = "создать оценку")
    HomeworkMarkDto createHomeworkMark(HomeworkMarkDto homeworkMarkDto);

    @Operation(method = "GET", description = "получить оценку по id")
    HomeworkMarkDto getHomeworkMarkById(Long id);

    @Operation(method = "GET", description = "получить все оценки студента")
    List<HomeworkMarkDto> getMarksByStudentId(Long studentId);

    @Operation(method = "PUT", description = "обновить оценку по id")
    HomeworkMarkDto updateHomeworkMarkById(Long id, HomeworkMarkDto homeworkMarkDto);

    @Operation(method = "DELETE", description = "удалить оценку по id")
    String deleteHomeworkMarkById(Long id);
}

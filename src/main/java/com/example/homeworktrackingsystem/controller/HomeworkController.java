package com.example.homeworktrackingsystem.controller;

import com.example.homeworktrackingsystem.domain.dto.HomeworkDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

@Tag(name = "домашние задания", description = "Контроллер для работы с домашними заданиями")
public interface HomeworkController {
    @Operation(method = "POST", description = "создать домашнее задание")
    HomeworkDto createHomework(HomeworkDto homeworkDto);

    @Operation(method = "GET", description = "получить домашнее задание по id")
    HomeworkDto getHomeWorkById(Long id);

    @Operation(method = "GET", description = "получить домашние задания по id студента")
    List<HomeworkDto> getHomeworksByStudentId(Long studentId);

    @Operation(method = "GET", description = "получить домашние задания по id учителя")
    List<HomeworkDto> getHomeworksByTeacherId(Long teacherId);

    @Operation(method = "PUT", description = "обновить домашнее задание по id")
    HomeworkDto updateHomeworkById(Long id, HomeworkDto homeworkDto);

    @Operation(method = "DELETE", description = "удалить домашнее задание по id")
    String deleteHomeworkById(Long id);
}

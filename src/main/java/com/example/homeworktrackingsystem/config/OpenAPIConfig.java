package com.example.homeworktrackingsystem.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {

    @Bean
    public OpenAPI libraryProoject() {
        return new OpenAPI()
                .info(new Info()
                        .title("Расписание")
                        .description("Сервис для отслеживания домашних заданий")
                        .version("v.0.0.1")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org"))
                        .contact(new Contact().name("Yrikov Valerii ")
                                .email("abuuu.bandit@mail.ru")
                                .url(""))
                        .contact(new Contact().name("Lepeshov Kirill")
                                .email("turkeyy565@mail.ru")
                                .url(""))
                        .contact(new Contact().name("Muhdin Dmitrii")
                                .email("vacbankick@gmail.com")
                                .url(""))
                        .contact(new Contact().name("Avralev Ivan")
                                .email("Avralyov03@mail.ru")
                                .url(""))
                        .contact(new Contact().name("Sedov Vasilii")
                                .email("insideded28@gmail.com")
                                .url(""))
                );
    }
}

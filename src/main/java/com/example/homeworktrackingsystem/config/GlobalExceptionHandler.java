package com.example.homeworktrackingsystem.config;

import com.example.homeworktrackingsystem.domain.exception.GroupNotFoundException;
import com.example.homeworktrackingsystem.domain.exception.HomeworkMarkNotFoundException;
import com.example.homeworktrackingsystem.domain.exception.HomeworkNotFoundException;
import com.example.homeworktrackingsystem.domain.exception.MarkForHomeworkAlreadyExistException;
import com.example.homeworktrackingsystem.domain.exception.StudentNotFoundException;
import com.example.homeworktrackingsystem.domain.exception.TeacherNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(StudentNotFoundException.class)
    public ResponseEntity<String> handleStudentNotFoundException(StudentNotFoundException ex) {
        log.error(ex.getMessage());
        return ResponseEntity.status(404).body(ex.getMessage());
    }

    @ExceptionHandler(GroupNotFoundException.class)
    public ResponseEntity<String> handleGroupNotFoundException(GroupNotFoundException ex) {
        log.error(ex.getMessage());
        return ResponseEntity.status(404).body(ex.getMessage());
    }

    @ExceptionHandler(TeacherNotFoundException.class)
    public ResponseEntity<String> handleTeacherNotFoundException(TeacherNotFoundException ex) {
        log.error(ex.getMessage());
        return ResponseEntity.status(404).body(ex.getMessage());
    }

    @ExceptionHandler(HomeworkNotFoundException.class)
    public ResponseEntity<String> handleHomeworkNotFoundException(HomeworkNotFoundException ex) {
        log.error(ex.getMessage());
        return ResponseEntity.status(404).body(ex.getMessage());
    }

    @ExceptionHandler(HomeworkMarkNotFoundException.class)
    public ResponseEntity<String> handleHomeworkMarkNotFoundException(HomeworkMarkNotFoundException ex) {
        log.error(ex.getMessage());
        return ResponseEntity.status(404).body(ex.getMessage());
    }

    @ExceptionHandler(MarkForHomeworkAlreadyExistException.class)
    public ResponseEntity<String> handleHomeworkMarkAlreadyExistException(MarkForHomeworkAlreadyExistException ex) {
        log.error(ex.getMessage());
        return ResponseEntity.status(400).body(ex.getMessage());
    }
}
